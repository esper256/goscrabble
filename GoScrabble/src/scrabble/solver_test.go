package scrabble

import (
	"testing"
	"fmt"
)

var (
	test_dict, _ = CreateDict([]string{"bogo", "cat", "cab", "can", "dog", "federated", "undue", "no", "bag", "oboes"})
	test_dict2, _ = CreateDict([]string{"bacons", "lasso", "os", "so"})
	test_solver  = CreateSolver(test_dict)
	test_solver2  = CreateSolver(test_dict2)
	test_board, _ = CreateBoard([][]rune{
		{' ', ' ', 'b', ' ', ' ', ' '},
		{' ', ' ', 'a', ' ', ' ', ' '},
		{' ', ' ', 'c', ' ', ' ', ' '},
		{' ', ' ', 'o', ' ', ' ', ' '},
		{' ', ' ', 'n', 'o', 's', 'e'},
		{' ', ' ', ' ', ' ', ' ', ' '},
		{' ', ' ', ' ', ' ', ' ', ' '},
		{' ', ' ', ' ', ' ', ' ', ' '},
	})
)

func assertSolFound(t *testing.T, x, y int, word string, moves []Move, b Board) {
	for _, m := range moves {
		placements := m.GetPlacements()
		wf := m.GetWord()
		if wf == word && x == placements[0].x && y == placements[0].y {
			return
		}
	}
	t.Errorf("Expected to find word '%v' but found solutions: '%v'", word, solutionSliceString(moves, b))
}

func getAllMoves(solc <-chan Move) []Move {
	sol := make([]Move, 0)

	for s := range solc {
		sol = append(sol, s)
	}
	return sol
}

func solutionSliceString(sol []Move, b Board) string {
	strs := make([]string, len(sol))
	for i, m := range sol {
		strs[i] = m.String()
	}
	return fmt.Sprintf("%v", strs)
}

func Test_FindsLeftWord(t *testing.T) {
	solc := test_solver.GetAllMoves(test_board, HandFromString("ca"))
	sol := getAllMoves(solc)
	assertSolFound(t, 0, 0, "cab", sol, test_board)
}

func Test_FindsRightWord(t *testing.T) {
	solc := test_solver.GetAllMoves(test_board, HandFromString("togto"))
	sol := getAllMoves(solc)
	if len(sol) != 1 {
		t.Errorf("Expected only one word but found %d", len(sol))
	}
	assertSolFound(t, 3, 0, "bogo", sol, test_board)
}

func Test_FindsVerticalWord(t *testing.T) {
	solc := test_solver.GetAllMoves(test_board, HandFromString("undu"))
	sol := getAllMoves(solc)
	assertSolFound(t, 5, 0, "undue", sol, test_board)
}

func Test_CannotConflictWithBoard(t *testing.T) {
	solc := test_solver.GetAllMoves(test_board, HandFromString("boes"))
	sol := getAllMoves(solc)
	if len(sol) != 0 {
		t.Errorf("Expected not to find any solutions but found %d", len(sol))
	}
}

func Test_FindAllWords(t *testing.T) {
	solc := test_solver.GetAllMoves(test_board, HandFromString("etaonrisbzc"))
	sol := getAllMoves(solc)
	if len(sol) != 8 {
		t.Errorf("Expected not to find any solutions but found %d", len(sol))
	}
}

func Test_FindParallelWord(t *testing.T) {
	solc := test_solver2.GetAllMoves(test_board, HandFromString("lasso"))
	sol := getAllMoves(solc)
	assertSolFound(t, 0, 5, "lasso", sol, test_board)
}