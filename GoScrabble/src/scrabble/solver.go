package scrabble

import (
	"log"
	"os"
	"sort"
	"sync"
)

var (
	l *log.Logger = log.New(os.Stdout, "", log.LstdFlags|log.Lshortfile)
)

type Solver interface {
	GetBestMoves(b Board, hand Tiles) <-chan Move
	GetAllMoves(b Board, hand Tiles) <-chan Move
	GetBestMovesFromPoint(b Board, hand Tiles, x int, y int) <-chan Move
	GetAllMovesFromPoint(b Board, hand Tiles, x int, y int) <-chan Move
}

type solver struct {
	dict Dictionary
}

func CreateSolver(dict Dictionary) Solver {
	s := solver{dict}
	return &s
}

func makeOutputChan() chan Move {
	return make(chan Move, 50)
}

func filterBestMoves(mc <-chan Move) <-chan Move {
	bestmovechan := makeOutputChan()
	go func(input <-chan Move) {
		w := new(sync.WaitGroup)
		bestmove := -1
		for m := range input {
			w.Add(1)
			go func() {
				if m.GetScore() > bestmove {
					bestmove = m.GetScore()
					bestmovechan <- m
				}
				w.Done()
			}()

		}
		go func() {
			w.Wait()
			close(bestmovechan)
		}()
	}(mc)
	return bestmovechan
}

func (s *solver) GetBestMoves(b Board, hand Tiles) <-chan Move {
	return filterBestMoves(s.GetAllMoves(b, hand))
}

func (s *solver) GetAllMoves(b Board, hand Tiles) <-chan Move {
	allmovechan := makeOutputChan()
	w := new(sync.WaitGroup)

	solverchans := []<-chan Move{
		s.getHorizontalMoves(b, hand), s.getHorizontalMoves(b.Rotate(), hand)}

	for _, sc := range solverchans {
		w.Add(1)
		go func(input <-chan Move) {
			for m := range input {
				allmovechan <- m
			}
			w.Done()
		}(sc)
	}

	go func() {
		w.Wait()
		close(allmovechan)
	}()

	return allmovechan
}

func (s *solver) GetAllMovesFromPoint(b Board, hand Tiles, x int, y int) <-chan Move {
	pointmovechan := makeOutputChan()
	w := new(sync.WaitGroup)
	w.Add(2)
	go func() {
		s.findAllHorizontalMovesFromPoint(b, hand, x, y, pointmovechan)
		w.Done()
	}()
	go func() {
		s.findAllHorizontalMovesFromPoint(b.Rotate(), hand, x, y, pointmovechan)
		w.Done()
	}()

	go func() {
		w.Wait()
		close(pointmovechan)
	}()

	return pointmovechan
}

func (s *solver) GetBestMovesFromPoint(b Board, hand Tiles, x int, y int) <-chan Move {
	return filterBestMoves(s.GetAllMovesFromPoint(b, hand, x, y))
}

func (s *solver) getHorizontalMoves(b Board, sharedhand Tiles) <-chan Move {
	dst := makeOutputChan()
	w := new(sync.WaitGroup)
	hand := make(Tiles, len(sharedhand))
	copy(hand, sharedhand)
	sort.Sort(hand)
	// Find all possible attachment points
	for y := 0; y < b.Height(); y++ {
		for x := 0; x < b.Width(); x++ {
			if b.Get(x, y) != NoTile {
				if x + 1 == b.Width() || b.Get(x+1,y) == NoTile {
					w.Add(1)
					go func(x2, y2 int) {
						s.findAllHorizontalMovesFromPoint(b, hand, x2, y2, dst)
						w.Done()
					}(x, y)
				}
			} else {
				if ((y+1 < b.Height() && b.Get(x, y+1) != NoTile) ||
					(y-1 >= 0 && b.Get(x, y-1) != NoTile)) && 
					(x+1 >= b.Width() || b.Get(x+1, y) == NoTile) &&
					(x-1 <= 0 || b.Get(x-1, y) == NoTile) {
					w.Add(1)
					go func(x2, y2 int) {
						s.findAllHorizontalMovesFromPoint(b, hand, x2, y2, dst)
						w.Done()
					}(x, y)
				}
			}
		}
	}

	go func() {
		w.Wait()
		close(dst)
	}()

	return dst
}

func (s *solver) findAllHorizontalMovesFromPoint(b Board, sharedhand Tiles, sx int, y int, dst chan<- Move) {
	//l.Printf("findAllHorizontalMovesFromPoint %d, %d", sx, y)
	// Defensive copy
	hand := make(Tiles, len(sharedhand))
	copy(hand, sharedhand)

	moves := 0
	tileused := make([]Tile, s.dict.LongestWord()+2)
	tileidx := make([]int, s.dict.LongestWord()+2)
	tilexpos := make([]int, s.dict.LongestWord()+2)
	tilenode := make([]WordTree, s.dict.LongestWord()+2)

	wn := s.dict.GetWordTreeRoot()

	dir := -1
	nexttileidx := 0
	x := sx

	checkvalidword := false
	var exists bool
	var nextnode WordTree

ml:
	for {
		//l.Printf("dir: % d, x: % 3d, [% 10s] hand: [%s]:%d : ", dir, x, TilesToString(tileused[0:moves]), hand.String(), nexttileidx)
		if x < 0 {
			// We are off the left edge of the board
			wn, exists = wn.NextTile(SwitchToRightEnd)
			if exists {
				dir = 1
				x = sx + 1
				//l.Printf("switching to right dir")
				continue ml
			} else {
				goto backtrack
			}
		}

		if x >= b.Width() {
			// We are off the right edge of the board
			checkvalidword = true
		} else {
			btile := b.Get(x, y)
			if btile == NoTile {
				// We can place one of our own tiles
			tilechoosing:
				for ; nexttileidx < len(hand); nexttileidx++ {
					t := hand[nexttileidx]
					if t == NoTile || (nexttileidx > 0 && hand[nexttileidx] == hand[nexttileidx-1]) {
						continue
					}

					nextnode, exists = wn.NextTile(t)
					
					if exists {
						// Ensure the tile does not conflict vertically.
						if (y > 0 && b.Get(x, y-1) != NoTile) || (y+1 < b.Height() && b.Get(x, y+1) != NoTile) {
							// Search the word tree for a word.
							var vertchecknode WordTree
							vertchecknode, exists = s.dict.GetWordTreeRoot().NextTile(t)
							if !exists {
								// Our chosen letter cannot go here.
								continue tilechoosing
							}
							
							for vy := y-1; vy >= 0; vy-- {
								nt := b.Get(x, vy)
								if nt == NoTile {
									break
								}
								vertchecknode, exists = vertchecknode.NextTile(nt)
								if !exists {
									// Our chosen letter cannot go here.
									continue tilechoosing
								}
							}
							vertchecknode, exists = vertchecknode.NextTile(SwitchToRightEnd)
							if !exists {
								// Our chosen letter cannot go here.
								continue tilechoosing
							}
							for vy := y+1; vy < b.Height(); vy++ {
								nt := b.Get(x, vy)
								if nt == NoTile {
									break
								}
								vertchecknode, exists = vertchecknode.NextTile(nt)
								if !exists {
									// Our chosen letter cannot go here.
									continue tilechoosing
								}
							}
							// We have tracked vertchecknode to the word that needs to be in the dictionary for this tile to work
							if !vertchecknode.IsWord() {
								// Our chosen letter cannot go here.
								continue tilechoosing
							}
						}

						// Record the move
						tileused[moves] = t
						tileidx[moves] = nexttileidx
						tilexpos[moves] = x
						tilenode[moves] = wn
						moves++
						// Mark the tile as used
						hand[nexttileidx] = NoTile
						// Advance our dictionary pointer
						wn = nextnode
						//l.Printf("placed tile '%s' at %d", t.String(), x)
						// Move to looking at the next spot on the board
						x += dir
						// Every time we look at a new place on the board we need to look at all tiles
						nexttileidx = 0
						continue ml // Continue on our main loop. Not the tile choosing loop
					}
				}
				// We've already tried every tile on this position.
				// If we were going left, then we need to consider not placing a tile, 
				// treating this as the left edge of the word and going right
				if dir == -1 {
					// Switch directions in the word tree
					//l.Printf("treating %d as end of the board", x)
					x = -1
					nexttileidx = 0
					continue ml // Continue on the main loop

					// Otherwise fall through to backtrack
				} else {
					// We've tried every tile in this position and we were already going right.
					// This means we should check to see if the word we had without placing another
					// tile is a word.
					checkvalidword = true
				} // Now we have to backtrack, since we've tried everything we could on this spot
			} else {
				// There's one on the board already!
				wn, exists = wn.NextTile(btile)
				if exists {
					//l.Printf("skipping over '%s' which was already on the board at %d", btile.String(), x)
					x += dir
					// Every time we look at a new place on the board we need to look at all tiles
					nexttileidx = 0
					continue ml // Continue on the main loop
				} // else it doesn't exist, is not a word and we have to backtrack
			}
		}
		// Backtrack one move
		// First see if we need to check for a valid move
		if checkvalidword {
			//l.Printf("valid checked: [%s] - ", TilesToString(tileused[0:moves]))
			if moves > 0 && wn.IsWord() {
				// We have a word
				placements := make([]Placement, moves)
				for i := 0; i < moves; i++ {
					placements[i].tile = tileused[i]
					placements[i].x, placements[i].y = b.UserCoords(tilexpos[i], y)
				}
				//l.Printf("Move found: %s", m.String())
				dst <- CreateMove(placements, b.GetUnrotated())
			}
			checkvalidword = false
		}
	backtrack:
		//l.Printf("Backtracking")
		// If we haven't made a move, we are done with our search
		if moves == 0 {
			//l.Printf("We've backtracked out of the problem.")
			return
		}
		moves--
		// Return the tile to our hand
		hand[tileidx[moves]] = tileused[moves]
		// Earmark the next tile to use
		nexttileidx = tileidx[moves] + 1
		// Return x to the position of the tile of the last play
		x = tilexpos[moves]
		if x <= sx {
			dir = -1
		}
		// Return the dictionary to the place we last checked
		wn = tilenode[moves]
	}
}
