package scrabble

import (
	"bytes"
	"errors"
	"fmt"
	"unicode"
	"strconv"
)

var (
	InvalidBoardDimensions = errors.New("scrabble: Invalid board dimensions")
	InvalidBonusType = errors.New("scrabble: Invalid bonus type")
	bonusPos = [][]string{
		{"3w", "", "", "2l", "", "", "", "3w", "", "", "", "2l", "", "", "3w"},
		{"", "2w", "", "", "", "3l", "", "", "", "3l", "", "", "", "2w", ""},
		{"", "", "2w", "", "", "", "2l", "", "2l", "", "", "", "2w", "", ""},
		{"2l", "", "", "2w", "", "", "", "2l", "", "", "", "2w", "", "", "2l"},
		{"", "", "", "", "2w", "", "", "", "", "", "2w", "", "", "", ""},
		{"", "3l", "", "", "", "3l", "", "", "", "3l", "", "", "", "3l", ""},
		{"", "", "2l", "", "", "", "2l", "", "2l", "", "", "", "2l", "", ""},
		{"3w", "", "", "2l", "", "", "", "2w", "", "", "", "2l", "", "", "3w"},
		{"", "", "2l", "", "", "", "2l", "", "2l", "", "", "", "2l", "", ""},
		{"", "3l", "", "", "", "3l", "", "", "", "3l", "", "", "", "3l", ""},
		{"", "", "", "", "2w", "", "", "", "", "", "2w", "", "", "", ""},
		{"2l", "", "", "2w", "", "", "", "2l", "", "", "", "2w", "", "", "2l"},
		{"", "", "2w", "", "", "", "2l", "", "2l", "", "", "", "2w", "", ""},
		{"", "2w", "", "", "", "3l", "", "", "", "3l", "", "", "", "2w", ""},
		{"3w", "", "", "2l", "", "", "", "3w", "", "", "", "2l", "", "", "3w"},
	}	
)

type Bonus struct {
	WordBonus  bool
	Multiplier int
}

type Board interface {
	Width() int
	Height() int
	UserCoords(x, y int) (int, int)
	Get(x, y int) Tile
	Bonus(x, y int) Bonus
	String() string
	Rotate() Board
	GetUnrotated() Board
	PlayMove(m Move)
}

type sliceBoard struct {
	rotated      bool
	width        int
	height       int
	tiles        []Tile
	bonus        []Bonus
	rotatedBoard *sliceBoard
}

func CreateOfficialScrabbleBoard() Board {
	b, err := CreateBlankBoard(15,15,bonusPos)
	if err != nil {
		panic(err)
	}
	return b
}

func (b *sliceBoard) Width() int {
	return b.width

}

func (b *sliceBoard) Height() int {
	return b.height
}

func (b *sliceBoard) UserCoords(x, y int) (int, int) {
	if b.rotated {
		return y, x
	}
	return x, y
}

func (b *sliceBoard) Get(x, y int) Tile {
	return b.tiles[y*b.width+x]
}

func (b *sliceBoard) Bonus(x, y int) Bonus {
	return b.bonus[y*b.width+x]
}

func (b *sliceBoard) String() string {
	var buffer bytes.Buffer
	fmt.Fprintf(&buffer, "%vx%v board:\n", b.Width(), b.Height())
	for y := 0; y < b.Height(); y++ {
		for x := 0; x < b.Width(); x++ {
			fmt.Fprint(&buffer, b.Get(x, y))
		}
		fmt.Fprintf(&buffer, "\n")
	}
	return buffer.String()
}

func (b *sliceBoard) PlayMove(m Move) {
	for _, p := range m.GetPlacements() {
		b.placeTile(p.tile, p.x, p.y)
	}
}

func (b *sliceBoard) GetUnrotated() Board {
	if b.rotated {
		return b.rotatedBoard
	}
	return b
}

func (b *sliceBoard) placeTile(t Tile, x int, y int) {
	b.tiles[y*b.width+x] = t
	if b.rotatedBoard != nil {
		b.rotatedBoard.tiles[x*b.rotatedBoard.width+y] = t
	}
}

func (b *sliceBoard) Rotate() Board {
	if b.rotatedBoard != nil {
		return b.rotatedBoard
	}

	rotatedBoard := new(sliceBoard)
	rotatedBoard.tiles = make([]Tile, b.Height()*b.Width())
	rotatedBoard.bonus = make([]Bonus, b.Height()*b.Width())
	rotatedBoard.width = b.Height()
	rotatedBoard.height = b.Width()
	rotatedBoard.rotated = true
	rotatedBoard.rotatedBoard = b

	for y := 0; y < rotatedBoard.height; y++ {
		for x := 0; x < rotatedBoard.width; x++ {
			rotatedBoard.tiles[y*rotatedBoard.width+x] = b.Get(y, x)
			rotatedBoard.bonus[y*rotatedBoard.width+x] = b.Bonus(y, x)
		}
	}

	b.rotatedBoard = rotatedBoard
	return rotatedBoard
}

func CreateBlankBoard(width int, height int, bonus [][]string) (*sliceBoard, error) {
	newBoard := new(sliceBoard)
	newBoard.width = width
	newBoard.height = height
	newBoard.tiles = make([]Tile, newBoard.height*newBoard.width)
	newBoard.bonus = make([]Bonus, newBoard.height*newBoard.width)
	for y := 0; y < newBoard.height; y++ {
		row := bonus[y]
		if len(row) != newBoard.width {
			return nil, InvalidBoardDimensions
		}
		for x := 0; x < newBoard.width; x++ {
			newBoard.tiles[y*newBoard.width+x] = NoTile
			bstr := bonus[y][x]
			if bstr != "" {
				btc := bstr[len(bstr)-1]
				switch btc {
					case 'l':
						// letter multiplier
						newBoard.bonus[y*newBoard.width+x].WordBonus = false
					case 'w':
						newBoard.bonus[y*newBoard.width+x].WordBonus = true
					default:
						return nil, InvalidBonusType
				}
				value, err := strconv.ParseInt(bstr[0:len(bstr)-1], 10, 32)
				if err != nil {return nil, err}
				newBoard.bonus[y*newBoard.width+x].Multiplier = int(value)
			}
		}
	}	
	return newBoard, nil
}

func CreateBoard(tiles [][]rune) (*sliceBoard, error) {
	newBoard := new(sliceBoard)
	// Assume Y is the first dimension of the array
	newBoard.height = len(tiles)
	if newBoard.height == 0 {
		return nil, InvalidBoardDimensions
	}
	newBoard.width = len(tiles[0])
	if newBoard.width == 0 {
		return nil, InvalidBoardDimensions
	}
	newBoard.tiles = make([]Tile, newBoard.height*newBoard.width)
	newBoard.bonus = make([]Bonus, newBoard.height*newBoard.width)

	for y := 0; y < newBoard.height; y++ {
		row := tiles[y]
		if len(row) != newBoard.width {
			return nil, InvalidBoardDimensions
		}
		for x := 0; x < newBoard.width; x++ {
			newBoard.tiles[y*newBoard.width+x] = FromRune(unicode.ToLower(row[x]))
		}
	}
	return newBoard, nil
}
