package scrabble

import (
	"fmt"
	"sort"
)

type Placement struct {
	x, y int
	tile Tile
}

func (p Placement) String() string {
	return fmt.Sprintf("%s@(%d,%d)", p.tile.String(), p.x, p.y)
}

type Move interface {
	GetPlacements() []Placement
	GetScore() int
	String() string
	GetWord() string
}

type move struct {
	placements []Placement
	board      Board
	score      int
}

func CreateMove(placements []Placement, board Board) Move {
	m := move{placements, board, 0}
	sort.Sort(&m)
	return &m
}

func (m *move) GetScore() int {
	if m.score == 0 {
		m.score = scoreMove(m.placements, m.board)
	}
	return m.score
}

func scoreMove(placements []Placement, b Board) int {
	if len(placements) == 1 {	
		return scoreSingleTileMove(placements[0], b)
	}
	adjscore := 0
	wordmult := 1

	horizontal := true
	if placements[0].x == placements[1].x {
		// Vertical word made.
		horizontal = false
	}

	wordscore := 0

	for _, p := range placements {
		tvalue := p.tile.Points()
		bonus := b.Bonus(p.x, p.y)
		linemult := 1
		if bonus.Multiplier != 0 {
			if bonus.WordBonus {
				linemult = bonus.Multiplier
				wordmult = wordmult * bonus.Multiplier
			} else {
				tvalue = tvalue * bonus.Multiplier
			}
		}
		// Get the score of the strip adjacent to the letter
		linescore := getTileLineScore(b, p.x, p.y, !horizontal)
		if linescore > 0 {linescore += tvalue}
		adjscore += (linescore * linemult)
		wordscore += tvalue
	}
	
	begin := placements[0]
	end := placements[len(placements) - 1]
	if horizontal {
		// Find minx
		var minx int
		for minx = begin.x; minx-1 > 0 && b.Get(minx-1,begin.y) != NoTile; minx-- {}
		var maxx int
		for maxx = end.x; maxx+1 < b.Width() && b.Get(maxx-1,end.y) != NoTile; maxx++ {}
		for x := minx; x <= maxx; x++ {
			if b.Get(x, begin.y) != NoTile {
				wordscore += b.Get(x, begin.y).Points()
			}
		}
	} else {
		// Find miny
		var miny int
		for miny = begin.y; miny-1 > 0 && b.Get(begin.x,miny-1) != NoTile; miny-- {}
		var maxy int
		for maxy = end.y; maxy+1 < b.Height() && b.Get(end.x,maxy+1) != NoTile; maxy++ {}
		for y := miny; y <= maxy; y++ {
			if b.Get(begin.x, y) != NoTile {
				wordscore += b.Get(begin.x, y).Points()
			}
		}
	}
	wholehand := 0
	if len(placements) >= 7 {
		wholehand = 50
	}
	return (wordscore * wordmult) + adjscore + wholehand
}

func getTileLineScore(b Board, x int, y int, horizontal bool) int {
	score := 0
	if horizontal {
		for xi := x-1; xi >= 0 && b.Get(xi, y) != NoTile; xi-- {
			score += b.Get(xi, y).Points()
		}
		for xi := x+1; xi < b.Width() && b.Get(xi, y) != NoTile; xi++ {
			score += b.Get(xi, y).Points()
		}
	} else {
		for yi := y-1; yi >= 0 && b.Get(x, yi) != NoTile; yi-- {
			score += b.Get(x, yi).Points()
		}
	
		for yi := y+1; yi < b.Height() && b.Get(x, yi) != NoTile; yi++ {
			score += b.Get(x, yi).Points()
		}	
	}
	return score
}

func scoreSingleTileMove(p Placement, b Board) int {
	mult := 1
	tvalue := p.tile.Points()
	bonus := b.Bonus(p.x, p.y)
	if bonus.Multiplier != 0 {
		if bonus.WordBonus {
			mult = bonus.Multiplier
		} else {
			tvalue = tvalue * bonus.Multiplier
		}
	}
	hscore := getTileLineScore(b, p.x, p.y, true)
	vscore := getTileLineScore(b, p.x, p.y, false)
	tscore := 0
	if hscore > 0 {tscore += tvalue}
	if vscore > 0 {tscore += tvalue}
	return (hscore + vscore + tscore) * mult
}

func (m *move) GetPlacements() []Placement {
	return m.placements
}

func (m *move) Len() int {
	return len(m.placements)
}

func (m *move) Swap(i, j int) {
	m.placements[i], m.placements[j] = m.placements[j], m.placements[i]
}

func (m *move) Less(i, j int) bool {
	if m.placements[i].x == m.placements[j].x {
		return m.placements[i].y < m.placements[j].y
	}
	return m.placements[i].x < m.placements[j].x
}

func (m *move) GetWord() string {
	b := m.board
	horizontal := true
	if len(m.placements) > 1 {
		if m.placements[0].x == m.placements[1].x {
			horizontal = false
		}
	} else {
		if m.placements[0].y-1 >= 0 && (b.Get(m.placements[0].x, m.placements[0].y-1) != NoTile) ||
			m.placements[0].y+1 < b.Height() && (b.Get(m.placements[0].x, m.placements[0].y+1) != NoTile) {
			horizontal = false
		}
	}
	wb := make([]rune, 0)

	if horizontal {
		y := m.placements[0].y
		minx := m.placements[0].x
		for ; minx > 0; minx-- {
			if b.Get(minx-1, y) == NoTile {
				break
			}
		}
		i := 0
		for x := minx; i < len(m.placements) || (x < b.Width() && b.Get(x, y) != NoTile); x++ {
			t := b.Get(x, y)
			if t == NoTile {
				wb = append(wb, m.placements[i].tile.AsRune())
				i++
			} else {
				wb = append(wb, t.AsRune())
			}
		}
	} else {
		x := m.placements[0].x
		miny := m.placements[0].y
		for ; miny > 0; miny-- {
			if b.Get(x, miny-1) == NoTile {
				break
			}
		}
		i := 0
		for y := miny; i < len(m.placements) || (y < b.Height() && b.Get(x, y) != NoTile); y++ {
			t := b.Get(x, y)
			if t == NoTile {
				wb = append(wb, m.placements[i].tile.AsRune())
				i++
			} else {
				wb = append(wb, t.AsRune())
			}
		}
	}
	return string(wb)
}

func (m *move) String() string {
	return fmt.Sprintf("%s @ %d,%d -> %d", m.GetWord(), m.placements[0].x, m.placements[0].y, m.GetScore())
}
