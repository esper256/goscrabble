package scrabble

import (
	"errors"
	"math/rand"
)

var (
	InvalidCharacter = errors.New("scrabble: Invalid character")
)

const NUM_TILES = 27

var (
	OfficialScrabbleTilePool = TilesFromString("eeeeeeeeeeeeaaaaaaaaaiiiiiiiiioooooooonnnnnnrrrrrrttttttllllssssuuuuddddgggbbccmmppffhhvvwwyykjxqz")
	runetotilemap = map[rune]Tile{
		'a': 0, 'b': 1, 'c': 2, 'd': 3, 'e': 4, 'f': 5, 'g': 6, 'h': 7, 'i': 8, 'j': 9,
		'k': 10, 'l': 11, 'm': 12, 'n': 13, 'o': 14, 'p': 15, 'q': 16, 'r': 17, 's': 18, 't': 19,
		'u': 20, 'v': 21, 'w': 22, 'x': 23, 'y': 24, 'z': 25,
	}
	runescores = []int{
		//ab  c  d  e  f  g  h  i  j  k  l  m  n  o  p  q   r  s  t  u  v  w  x  y  z
		1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10,
	}
	NoTile Tile = 26
	SwitchToRightEnd Tile = 26
	BlankTile Tile = 27
)

type Tile uint8
type Tiles []Tile

func (h Tiles) Len() int {
	return len(h)
}

func (h Tiles) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

func (h Tiles) Less(i, j int) bool {
	return h[i] < h[j]
}

func FromRune(r rune) Tile {
	t, ok := runetotilemap[r]
	if !ok {
		if r == '_' { return BlankTile }
		if r == ' ' { return NoTile }
		
		panic(errors.New("scrabble: Invalid character " + string(r)))
	}
	return t
}

func (t Tile) AsRune() rune {
	if t == BlankTile { return '_' }
	if t == NoTile { return ' ' }
	return 'a' + rune(t)
}

func (t Tile) String() string {
	return string(t.AsRune())
}

func (t Tile) Points() int {
	return runescores[t]
}

func TilesToString(t []Tile) string {
	r := make([]rune, len(t))
	for i := range t {
		r[i] = t[i].AsRune()
	}
	return string(r)
}

func (h Tiles) String() string {
	r := make([]rune, len(h))
	for i, t := range h {
		r[i] = t.AsRune()
	}
	return string(r)
}

func TilesFromString(s string) Tiles {
	h := make(Tiles, len(s))
	for i, c := range s {
		h[i] = FromRune(c)
	}
	return h
}

func (pool Tiles) ShuffleTilePool() {
	tmp := make(Tiles, len(pool))
	copy(tmp, pool)
	perm := rand.Perm(len(pool))
	for i, v := range perm {
	    pool[i] = tmp[v]
	}
}

func (pool Tiles) DrawTiles(num int) (remaining Tiles, drawn Tiles) {
	if num > len(pool) {
		num = len(pool)
	}
	return pool[num:], pool[:num]
}

func (pool Tiles) RemovePlayedTiles(m Move) Tiles {
	tmp := make(Tiles, len(pool))
	copy(tmp, pool)
	for _, p := range m.GetPlacements() {
		for i := range tmp {
			if tmp[i] == p.tile {
				tmp[i] = NoTile
			}
		}
	}
	remaining := make(Tiles, len(pool) - len(m.GetPlacements()))
	ri := 0
	for i := range tmp {
		if tmp[i] != NoTile {
			remaining[ri] = tmp[i]
		}
	}
	return remaining
}