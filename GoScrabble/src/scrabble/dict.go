package scrabble

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
)

const dictName = "words.txt"

var (
	ErrDictReadError = errors.New("scrabble: Couldn't read wordlist")
)

type WordTree interface {
	IsWord() bool
	NextTile(t Tile) (WordTree, bool)
	String() string
}

type Dictionary interface {
	IsWord([]Tile) bool
	GetWordTreeRoot() WordTree
	NumWords() int
	LongestWord() int
}

type node struct {
	isWord   bool
	children [NUM_TILES]*node
}

type treeDict struct {
	root     *node
	numwords int
	longestword int
}

func (n *node) String() string {
	count := 0
	for i := Tile(0); i < SwitchToRightEnd; i++ {
		if n.children[i] != nil {
			count++
		}
	}
	rs := make([]rune, count)
	count = 0
	for i := Tile(0); i < SwitchToRightEnd; i++ {
		if n.children[i] != nil {
			rs[count] = i.AsRune()
			count++
		}
	}
	arrow := ""
	if n.children[SwitchToRightEnd] != nil {
		arrow = " ->"
	}
	return fmt.Sprintf("%v node: %s%s", n.isWord, string(rs), arrow)
}

func (n *node) IsWord() bool {
	return n.isWord
}

func (n *node) NextTile(t Tile) (WordTree, bool) {
	if n.children[t] == nil {
		return nil, false
	}
	return n.children[t], true
}

func (d *treeDict) IsWord(ts []Tile) bool {
	var exists bool
	wt := d.GetWordTreeRoot()
	for i := len(ts) - 1; i >= 0; i-- {
		wt, exists = wt.NextTile(ts[i])
		if !exists {
			return false
		}
	}
	return wt.IsWord()
}

func (d *treeDict) GetWordTreeRoot() WordTree {
	return d.root
}

func (d *treeDict) NumWords() int {
	return d.numwords
}

func (d *treeDict) LongestWord() int {
	return d.longestword
}

func (d *treeDict) addWord(w string) {
	root := d.root
	d.numwords++
	if len(w) > d.longestword {
		d.longestword = len(w)
	}
	defer func() {
		recover()
	}()
	ts := TilesFromString(w)
	for i := range ts {
		root.addStartLetter(ts, i)
	}
}

func (r *node) addStartLetter(ts []Tile, start int) {
	n := r
	for i := start; i >= 0; i-- {
		n = n.getOrAddNode(ts[i])
	}
	// Throw in the right direction
	n = n.getOrAddNode(SwitchToRightEnd)
	for i := start + 1; i < len(ts); i++ {
		n = n.getOrAddNode(ts[i])
	}
	n.isWord = true
}

func (n *node) getOrAddNode(r Tile) *node {
	if n.children[r] == nil {
		n.children[r] = createNode()
	}
	return n.children[r]
}

func createNode() *node {
	n := new(node)
	return n
}

func CreateDict(words []string) (Dictionary, error) {
	dict := new(treeDict)
	dict.root = createNode()
	
	for _, w := range words {
		word := strings.ToLower(w)
		dict.addWord(word)
	}
	return dict, nil
}

func ReadDict() (Dictionary, error) {


	wordsfile, err := os.Open(dictName)
	if err != nil {
		panic(err)
	}
	defer wordsfile.Close()

	r := bufio.NewReader(wordsfile)
	lines := make([]string, 0, 20000)
	for {
		line, err := r.ReadString('\n')
		if err == io.EOF {
			return CreateDict(lines)
		}
		line = line[:len(line)-1]
		lines = append(lines, line)
	}

	return nil, ErrDictReadError
}
