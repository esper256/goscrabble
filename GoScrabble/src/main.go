package main

import (
	"fmt"
	"scrabble"
	"runtime"
	"math/rand"
	"time"
)

func getBestMove(c <-chan scrabble.Move) scrabble.Move {
	var m scrabble.Move
	for m = range c {}
	return m
}

func main() {
	fmt.Printf("Initializing...\n")
	dict, err := scrabble.ReadDict()
	if err != nil {
		panic(err)
	}
	fmt.Printf("%v words read.\n", dict.NumWords())

	ms := new(runtime.MemStats)
	var hand scrabble.Tiles
	solver := scrabble.CreateSolver(dict)

	runtime.ReadMemStats(ms)
	fmt.Printf("Memory usage after initialization: %d\n", ms.Alloc)
	rand.Seed(time.Now().UnixNano())
	fmt.Printf("Shuffling the tiles\n")

	prev := runtime.GOMAXPROCS(4)
	fmt.Printf("Previously up on %v maxprocs\n", prev)

	starttime := time.Now()
	for i := 0; i < 1000; i++ {
		board := scrabble.CreateOfficialScrabbleBoard()
		//fmt.Printf("New test board:\n%v", board)
	
		pool := make(scrabble.Tiles, len(scrabble.OfficialScrabbleTilePool))
		copy(pool, scrabble.OfficialScrabbleTilePool)
		pool.ShuffleTilePool()
		pool, hand = pool.DrawTiles(7)
		m := getBestMove(solver.GetBestMovesFromPoint(board, hand, 7, 7))
		if m == nil {
			fmt.Printf("No valid first moves with hand: %v\n", hand)
			continue
		}
		
		board.PlayMove(m)
		hand = hand.RemovePlayedTiles(m)
		for (m != nil || len(pool) > 0 && len(hand) < 7) && (len(pool) > 0 || len(hand) > 0) {
			//fmt.Printf("Board state:\n%v", board)
			var replacements scrabble.Tiles
			pool, replacements = pool.DrawTiles(7-len(hand))
			for _, replacement := range replacements {
				hand = append(hand, replacement)
			}
			m = getBestMove(solver.GetBestMoves(board, hand))
			if m == nil {
				//fmt.Printf("No valid moves with hand: %v\n", hand)
			} else {
				board.PlayMove(m)
				hand = hand.RemovePlayedTiles(m)
			}
		}
	}
	endtime := time.Now()
	fmt.Printf("\nAll finished -- no moves found. Done in %f\n", endtime.Sub(starttime).Seconds())
}
